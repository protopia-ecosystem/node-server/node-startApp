const Utils = {
  promisify(array, query, document) {
    const promises = [];

    if (query && !document) {
      document = query;
    }

    array.forEach((promisedCallback) => {
      promises.push(promisedCallback(document));
    });

    if (array.length <= 0) {
      promises.push(new Promise((resolve, reject) => {
        reject = reject || null; // to avoid jshint unused code
        return resolve(document);
      }));
    }

    return promises;
  },

  setDatetime(opName, datetime, document) {
    if (datetime) {
      if (opName === 'update') {
        if (!document.$set && !document.$setOnInsert) {
          document.$set = document.$set || {};
          document.$set.updatedAt = new Date();
        } else {
          document.$set = document.$set || {};
          document.$set.updatedAt = new Date();

          //          document.$setOnInsert = document.$setOnInsert || {};
          //          document.$setOnInsert.createdAt = new Date();
        }
      } else if (opName === 'insert') {
        document.createdAt = new Date();
        document.updatedAt = new Date();
      }
    }
    return document;
  },

  setParams(opName, query, sort, docToProcess, options) {
    const params = [];

    switch (opName) {
      case 'update':
        params.push(query, docToProcess, options);
        break;
      case 'findAndModify':
        const data = {};
        if (query) data.query = query;
        if (sort) data.sort = sort;
        if (docToProcess) data.update = docToProcess;
        // if (options) data.options = options;
        params.push(data);
        break;
      default:
        params.push(docToProcess, options);
        break;
    }

    return params;
  },

  processUpdatedDocment(opName, collection, isAfterHook, query, mongObject, options) {
    if (opName === 'update' && mongObject.result.ok && isAfterHook) {
      const objToReturn = {
        value: [],
        lastErrorObject: {},
      };

      return collection.find(query)
        .toArray()
        .then((updatedDocs) => {
          if (!updatedDocs.length) {
            return mongObject;
          }
          objToReturn.value = (options && options.multi && mongObject.result.n > 1)
            ? updatedDocs : updatedDocs[0];

          objToReturn.lastErrorObject.updatedExisting = !!(mongObject.result.nModified);

          objToReturn.lastErrorObject.n = mongObject.result.n;

          if (!mongObject.result.nModified && mongObject.result.upserted) {
            objToReturn.lastErrorObject.upserted = updatedDocs[0]._id;
          }

          objToReturn.ok = mongObject.result.ok;

          return objToReturn;
        });
    }
    /* jshint unused: false */
    return new Promise((resolve, reject) => {
      resolve(mongObject);
    });
  },

  getHooksTemplate() {
    return {
      before: { insert: [], update: [], remove: [] },
      after: { insert: [], update: [], remove: [] },
    };
  },
};

// export Utils helper
module.exports = Utils;
