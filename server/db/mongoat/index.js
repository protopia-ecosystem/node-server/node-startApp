// modules dependencies
const mongodb = require('mongoist');
const utilsHelper = require('./utils-helper');

/**
 * @namespace Mongoat
 */
const Mongoat = mongodb;

/**
 * @private
 * @constructor
 * @description to initialize some properties
 */
Mongoat.Collection.prototype.init = function () {
  this.datetime = {};
  this.hooks = {};
  this.versioning = {};
  this.hooks = {};
};

(function () {
  const opMethod = ['insert', 'update', 'findAndModify', 'remove'];
  const colPrototype = Mongoat.Collection.prototype;

  colPrototype.init();

  for (let i = 0; i < opMethod.length; ++i) {
    colPrototype[`${opMethod[i]}Method`] = colPrototype[opMethod[i]];
  }
}());

/**
 * @function before
 * @memberOf Mongoat
 * @description add before hook to Collection
 * @param {String} opName - operation name, can be insert/update/findAndModify or remove
 * @param {Function} callback - the callback to run after the before hook
 * @return {Void} Nothing
 */
Mongoat.Collection.prototype.before = function (opName, callback) {
  this.hooks[this.name] = this.hooks[this.name]
    || utilsHelper.getHooksTemplate();

  this.hooks[this.name].before[opName].push(callback);
};

/**
 * @function after
 * @memberOf Mongoat
 * @description add after hook to Collection
 * @param {String} opName - operation name, can be insert/update/findAndModify or remove
 * @param {Function} callback - the callback to run after the after hook
 * @return {Void} Nothing
 */
Mongoat.Collection.prototype.after = function (opName, callback) {
  this.hooks[this.name] = this.hooks[this.name]
    || utilsHelper.getHooksTemplate();

  this.hooks[this.name].after[opName].push(callback);
};

/**
 * @function insert
 * @memberOf Mongoat
 * @description overwrite the insert method
 * @param {Object} document - document to insert
 * @param {Object} options - options used on insert
 * @return {Promise} - returned mongodb object after insert
 */
Mongoat.Collection.prototype.insert = function (document, options, callback) {
  return this.query('insert', undefined, undefined, document, options, callback);
};

/**
 * @function update
 * @memberOf Mongoat
 * @description overwrite the update method
 * @param {Object} query - query to match
 * @param {Object} update - document to update
 * @param {Object} options - options used on update
 * @return {Promise} - returned mongodb object after update
 */
Mongoat.Collection.prototype.update = function (query, update, options, callback) {
  return this.query('update', query, undefined, update, options, callback);
};

/**
 * @function findAndModify
 * @memberOf Mongoat
 * @description overwrite the findAndModify method
 * @param {Object} query - query to match
 * @param {Object} sort - sort rules
 * @param {Object} update - document to update
 * @param {Object} options - options used on update
 * @return {Promise} - returned mongodb object after findAndModify
 */
Mongoat.Collection.prototype.findAndModify = function (data) {
  return this.query('findAndModify', data.query, data.sort, data.update, data.options, data.callback);
};

/**
 * @function remove
 * @memberOf Mongoat
 * @description overwrite the remove method
 * @param {Object} query - query to match
 * @param {Object} options - options used on update
 * @return {Promise} - returned mongodb object after remove
 */
Mongoat.Collection.prototype.remove = function (query, options, callback) {
  return this.query('remove', query, undefined, undefined, options, callback);
};

/**
 * @function query
 * @memberOf Mongoat
 * @description to make any query, can be insert/update/findAndModify or remove
 * @param {String} opName - operation name, can be insert/update/findAndModify or remove
 * @param {Object} query - query to match
 * @param {Object} sort - sort rules
 * @param {Object} update - document to update
 * @param {Object} options - options used on update
 * @return {Promise} - returned mongodb object after query depending on opName
 */
Mongoat.Collection.prototype.query = function (opName, query, sort, document, options, callback) {
  let promises = [];
  const _this = this;
  const colName = _this.name;
  let opName_X;

  _this.hooks[colName] = _this.hooks[colName] || utilsHelper.getHooksTemplate();

  if (options && typeof options === 'function') {
    callback = options;
    options = {};
  }

  options = options || {};
  opName_X = (opName !== 'findAndModify') ? opName : 'update';
  utilsHelper.setDatetime(opName_X, true || _this.datetime[colName], document);
  promises = utilsHelper.promisify(_this.hooks[colName].before[opName_X], query, document);

  if (true || _this.versioning[colName]) {
    promises.push(_this.commit(opName, query, document, options));
  }

  return Promise.all(promises)
    .then((docToProcess) => {
      const params = utilsHelper.setParams(opName, query, sort, docToProcess[0], options);

      return _this[`${opName}Method`].apply(_this, params)
        .then((mongObject) => {
          const isAfterHook = (_this.hooks[colName].after[opName_X].length > 0);

          return utilsHelper.processUpdatedDocment(opName, _this, isAfterHook, query, mongObject, options)
            .then((obj) => {
              promises = [];
              promises = utilsHelper.promisify(_this.hooks[colName].after[opName_X], obj);
              Promise.all(promises);

              if (callback && typeof callback === 'function') {
                callback(null, mongObject);
              }

              return mongObject;
            });
        });
    })
    .catch((err) => {
      if (callback && typeof callback === 'function') {
        callback(err, null);
      } else {
        throw err;
      }
    });
};

/**
 * @function datetime
 * @memberOf Mongoat
 * @description to enable datetime feature
 * @param {Boolean} isEnabled - to enable or disable feature
 * @return {Void} Nothing
 */
Mongoat.Collection.prototype.datetime = function (isEnabled) {
  if (typeof isEnabled === 'boolean') {
    this.datetime[this.name] = isEnabled;
  }
};

/**
 * @function version
 * @memberOf Mongoat
 * @description to enable versioning feature
 * @param {Boolean} isEnabled - to enable or disable feature
 * @return {Void} Nothing
 */
Mongoat.Collection.prototype.version = function (isEnabled) {
  if (typeof isEnabled === 'boolean') {
    this.versioning[this.name] = isEnabled;
  }
};

/**
 * @function commit
 * @memberOf Mongoat
 * @description to commit document version
 * @param {String} opName - operation name, can be insert/update or remove
 * @param {Object} query - query to match
 * @param {Object} document - document to commit
 * @param {Object} options - options used on update
 * @return {Promise} - the document to insert/update or remove
 */
Mongoat.Collection.prototype.commit = function (opName, query, document, options) {
  if (opName === 'insert') {
    document._version = 1;
    return document;
  }

  const _this = this;
  const shadowCol = _this.db.collection(`${_this.name}.vermongo`);

  shadowCol.datetime(true || _this.datetime[_this.name]);

  /* jshint maxcomplexity:10 */
  return _this.findOne(query)
    .then((docToProcess) => {
      if (docToProcess) {
        const id = docToProcess._id;
        docToProcess._version = docToProcess._version || 1;
        docToProcess.document_id = { _id: id, _version: docToProcess._version };
        delete docToProcess._id;
      }
      if (opName === 'update' || opName === 'findAndModify') {
        if (!docToProcess && options.upsert) {
          if (!document.$setOnInsert) {
            document._version = 1;
          } else {
            document.$setOnInsert._version = 1;
          }

          return document;
        }
      } else if (opName === 'remove') {
        if (!docToProcess) {
          return query;
        }
        docToProcess._version = `deleted:${docToProcess.document_id._version}`;
      }
      return shadowCol.insertMethod(docToProcess)
        .then(() => {
          if (opName === 'update' || opName === 'findAndModify') {
            if (!document.$setOnInsert && !document.$set) {
              document._version = docToProcess.document_id._version + 1;
            } else {
              document.$set = document.$set || {};
              document.$set._version = docToProcess.document_id._version + 1;
              // document.$setOnInsert = document.$setOnInsert || {};
              // document.$setOnInsert._version = docToProcess._id._version + 1;
            }
            return document;
          } if (opName === 'remove') {
            return query;
          }
        });
    });
};

/**
 * @function restore
 * @memberOf Mongoat
 * @description to resotre document by version, can +v => version, 0 => last version or -v => last version - v
 * @param {Object} id - id of the document to restore
 * @param {Number} version - version of the document to restore
 * @return {Promise} - the restored document
 */
Mongoat.Collection.prototype.restore = function (id, version) {
  const col = this.db.collection(this.name);
  const shadowCol = this.db.collection(`${this.name}.vermongo`);
  const aggregatePipeline = [{ $match: { 'document_id._id': id } }];

  if (version <= 0) {
    aggregatePipeline.push({ $sort: { 'document_id._version': -1 } });
    aggregatePipeline.push({ $skip: Math.abs(version) });
  } else {
    aggregatePipeline.push({ $match: { 'document_id._version': version } });
  }

  aggregatePipeline.push({ $limit: 1 });

  return shadowCol.aggregate(aggregatePipeline)
    .nextObject()
    .then((document) => {
      if (!document) {
        return document;
      }

      const id = document.document_id._id;
      delete document.document_id;
      delete document.id;

      return col.update({ _id: id }, document, { upsert: true })
        .then((mongObject) => {
          if (mongObject.result.ok === 1 && mongObject.result.n === 1) {
            return document;
          }
        });
    });
};

// export Mongoat object
module.exports = Mongoat;
